import logging

from django.db.models import Count
from django.utils import timezone
from drf_spectacular.utils import extend_schema, extend_schema_view
from rest_framework import status, viewsets
from rest_framework.generics import get_object_or_404
from rest_framework.response import Response
from rest_framework.views import APIView

from .models import Client, Distribution, Message
from .serializers import ClientSerializer, DistributionSerializer

logger = logging.getLogger('main')


@extend_schema_view(
    list=extend_schema(summary='Список рассылок'),
    create=extend_schema(summary='Создание рассылки'),
    retrieve=extend_schema(summary='Детальные данные рассылки'),
    update=extend_schema(summary='Изменение рассылки'),
    partial_update=extend_schema(summary='Частичное изменение рассылки'),
    destroy=extend_schema(summary='Удаление рассылки'),
)
class DistributionView(viewsets.ModelViewSet):
    queryset = Distribution.objects.all()
    serializer_class = DistributionSerializer

    def perform_create(self, serializer):
        distribution = serializer.save()
        logger.info(f'DISTRIBUTION|created {distribution.id}')

    def perform_update(self, serializer):
        distribution = serializer.save()
        logger.info(f'DISTRIBUTION|updated {distribution.id}')

    def perform_destroy(self, instance):
        distribution_id = instance.id
        instance.delete()
        logger.info(f'DISTRIBUTION|updated {distribution_id}')


@extend_schema_view(
    list=extend_schema(summary='Список клиентов'),
    create=extend_schema(summary='Создание клиента'),
    retrieve=extend_schema(summary='Данные клиента'),
    update=extend_schema(summary='Изменение клиента'),
    partial_update=extend_schema(summary='Частичное изменение клиента'),
    destroy=extend_schema(summary='Удаление клиента'),
)
class ClientView(viewsets.ModelViewSet):
    queryset = Client.objects.all()
    serializer_class = ClientSerializer

    def perform_create(self, serializer):
        client = serializer.save()
        logger.info(f'CLIENT|created {client.id}')

    def perform_update(self, serializer):
        client = serializer.save()
        logger.info(f'CLIENT|updated {client.id}')

    def perform_destroy(self, instance):
        client_id = instance.id
        instance.delete()
        logger.info(f'CLIENT|updated {client_id}')


class TotalStatisticsAPIView(APIView):
    """ Получение общей статистики по всем рассылкам
    """

    @extend_schema(summary='Общая статистика по всем рассылкам')
    def get(self, request):
        messages_statistics = {}
        for messages in Message.objects.values('status').annotate(count=Count('id')):
            messages_statistics[messages['status']] = messages['count']

        return Response({
            'distributions_total': Distribution.objects.filter(start__lt=timezone.now()).count(),
            'messages_total': sum(messages_statistics.values()),
            'sending': messages_statistics.get('sending', 0),
            'delivered': messages_statistics.get('delivered', 0),
            'missed_deadline': messages_statistics.get('missed_deadline', 0),
            'timezone_conflict': messages_statistics.get('timezone_conflict', 0),
            'failed': messages_statistics.get('failed', 0),
        })


class DetailStatisticsAPIView(APIView):
    """ Получение статистики по одной рассылке
    """

    @extend_schema(summary='Статистика по одной рассылке')
    def get(self, request, pk: int):
        distribution = get_object_or_404(Distribution, pk=pk)

        messages_statistics = {}
        for messages in Message.objects.filter(distribution=distribution) \
                .values('status').annotate(count=Count('id')):
            messages_statistics[messages['status']] = messages['count']

        return Response({
            'messages_total': sum(messages_statistics.values()),
            'sending': messages_statistics.get('sending', 0),
            'delivered': messages_statistics.get('delivered', 0),
            'missed_deadline': messages_statistics.get('missed_deadline', 0),
            'timezone_conflict': messages_statistics.get('timezone_conflict', 0),
            'failed': messages_statistics.get('failed', 0),
        })


class HealthView(APIView):
    @extend_schema(exclude=True)
    def get(self, request, *args, **kwargs):
        # для проверки готовности docker контейнера
        return Response({'status': 'ok'}, status=status.HTTP_200_OK)
