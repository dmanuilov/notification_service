from django import forms
from django.core.exceptions import ValidationError
from django.utils import timezone

from .models import Distribution


class DistributionForm(forms.ModelForm):
    class Meta:
        model = Distribution
        fields = '__all__'

    def clean(self):
        cleaned_data = super().clean()

        start = cleaned_data.get('start') or timezone.localtime()
        end = cleaned_data.get('end')
        if end and start > end:
            raise ValidationError('Убедитесь, что начало рассылки раньше окончания.')

        return cleaned_data
