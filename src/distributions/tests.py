from django.utils import timezone
from rest_framework import status
from rest_framework.reverse import reverse
from rest_framework.test import APITestCase

from notification_service.celery import app

from .models import Client, Distribution, Message


class DistributionTests(APITestCase):
    def setUp(self):
        self._client = Client.objects.create(phone_number='+12345678900')
        app.conf.update(CELERY_ALWAYS_EAGER=True)

    def test_create_distribution(self):
        response = self.client.post(
            path=reverse('distribution-list'),
            data={
                'text': 'Test',
            },
            format='json'
        )
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(Distribution.objects.count(), 1)

        # проверка отправленного сообщения по рассылке
        self.assertEqual(Message.objects.count(), 1)

    def test_get_detail_distribution(self):
        new_distribution = Distribution.objects.create(
            text='Test'
        )

        response = self.client.get(
            path=reverse('distribution-detail', kwargs={'pk': new_distribution.pk})
        )
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertDictEqual(
            response.json(),
            {
                'id': new_distribution.id,
                'text': new_distribution.text,
                'code_filter': new_distribution.code_filter,
                'tag_filter': new_distribution.tag_filter,
                'start': timezone.localtime(new_distribution.start).isoformat(),
                'end': timezone.localtime(new_distribution.end).isoformat() if new_distribution.end else None,
                'allowed_start_hour': new_distribution.allowed_start_hour,
                'allowed_end_hour': new_distribution.allowed_end_hour,
            }
        )


class ClientTests(APITestCase):
    def test_create_client(self):
        response = self.client.post(
            path=reverse('client-list'),
            data={
                'phone_number': '+12345678900',
            },
            format='json'
        )
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(Client.objects.count(), 1)

    def test_get_detail_client(self):
        new_client = Client.objects.create(
            phone_number='+12345678900'
        )

        response = self.client.get(
            path=reverse('client-detail', kwargs={'pk': new_client.pk})
        )
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertDictEqual(
            response.json(),
            {
                'id': new_client.id,
                'phone_number': new_client.phone_number,
                'code': new_client.code,
                'tag': new_client.tag,
                'timezone': new_client.timezone,
            }
        )


class StatisticsTests(APITestCase):
    def setUp(self):
        self.distribution = Distribution.objects.create(text='Test')
        self._client = Client.objects.create(phone_number='+12345678900')
        Message.objects.create(
            status=Message.Status.SENDING,
            distribution=self.distribution,
            client=self._client,
        )

    def test_total_statistics(self):
        response = self.client.get(
            path=reverse('total-statistics')
        )
        self.assertEqual(response.status_code, status.HTTP_200_OK)

        data = response.json()
        self.assertIn('distributions_total', data)
        self.assertIn('messages_total', data)
        self.assertIn('sending', data)
        self.assertIn('delivered', data)
        self.assertIn('missed_deadline', data)
        self.assertIn('timezone_conflict', data)
        self.assertIn('failed', data)

    def test_detail_statistics(self):
        response = self.client.get(
            path=reverse('detail-statistics', kwargs={'pk': self.distribution.pk})
        )
        self.assertEqual(response.status_code, status.HTTP_200_OK)

        data = response.json()
        self.assertIn('messages_total', data)
        self.assertIn('sending', data)
        self.assertIn('delivered', data)
        self.assertIn('missed_deadline', data)
        self.assertIn('timezone_conflict', data)
        self.assertIn('failed', data)
