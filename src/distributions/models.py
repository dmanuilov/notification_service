from typing import Optional

import pytz as pytz
from django.conf import settings
from django.core.validators import MaxValueValidator, MinValueValidator
from django.db import models
from django.utils import timezone
from phonenumber_field.modelfields import PhoneNumberField

TZ_CHOICES = [(tz, tz) for tz in pytz.all_timezones]


class Distribution(models.Model):
    text = models.TextField('Текст')
    code_filter = models.PositiveSmallIntegerField('Фильтр по коду мобильного оператора', blank=True, null=True,
                                                   validators=[MinValueValidator(100), MaxValueValidator(999)])
    tag_filter = models.CharField('Фильтр по тегу', max_length=128, blank=True, null=True)
    start = models.DateTimeField('Запуск рассылки', default=timezone.now)
    end = models.DateTimeField('Окончание рассылки', blank=True, null=True)
    allowed_start_hour = models.PositiveSmallIntegerField('Разрешенный час с', default=8,
                                                          validators=[MaxValueValidator(24)])
    allowed_end_hour = models.PositiveSmallIntegerField('Разрешенный час до', default=22,
                                                        validators=[MaxValueValidator(24)])

    class Meta:
        verbose_name = 'Рассылка'
        verbose_name_plural = 'Рассылки'

    def __str__(self):
        return f'Рассылка №{self.pk} - {timezone.localtime(self.start).strftime("%d/%m/%Y %H:%M")}'


class ClientQuerySet(models.QuerySet):
    def match(self, code: Optional[int], tag: Optional[str]):
        clients = self
        if code:
            clients = clients.filter(code=code)
        if tag:
            clients = clients.filter(tag__icontains=tag)
        return clients


class Client(models.Model):
    objects = ClientQuerySet.as_manager()

    phone_number = PhoneNumberField('Номер телефона', unique=True)
    code = models.PositiveSmallIntegerField('Код мобильного оператора', blank=True,
                                            validators=[MinValueValidator(100), MaxValueValidator(999)])
    tag = models.CharField('Тег', max_length=128, blank=True, null=True)
    timezone = models.CharField('Часовой пояс', max_length=128, choices=TZ_CHOICES,
                                default=settings.TIME_ZONE)

    class Meta:
        verbose_name = 'Клиент'
        verbose_name_plural = 'Клиенты'

    def save(self, *args, **kwargs):
        # получение кода оператора из номера телефона
        if self.code is None:
            self.code = int(str(self.phone_number)[-10:-7])
        super().save(*args, **kwargs)

    def __str__(self):
        return str(self.phone_number)[1:]


class Message(models.Model):
    class Status(models.TextChoices):
        SENDING = 'sending', 'В процессе отправки'
        DELIVERED = 'delivered', 'Доставлено'
        MISSED_DEADLINE = 'missed_deadline', 'Не отправлено (просрочено)'
        TIMEZONE_CONFLICT = 'timezone_conflict', 'Не отправлено (неподходящее время)'
        FAILED = 'failed', 'Ошибка отправки'

    status = models.CharField('Статус', max_length=20, choices=Status.choices)
    created = models.DateTimeField('Отправлено', auto_now_add=True)
    distribution = models.ForeignKey(Distribution, on_delete=models.CASCADE,
                                     verbose_name='Рассылка', related_name='messages')
    client = models.ForeignKey(Client, on_delete=models.CASCADE,
                               verbose_name='Клиент', related_name='messages')

    class Meta:
        verbose_name = 'Сообщение'
        verbose_name_plural = 'Сообщения'

    def __str__(self):
        return f'Сообщение №{self.pk} - {timezone.localtime(self.created).strftime("%d/%m/%Y %H:%M")}'
