from django.contrib import admin

from .forms import DistributionForm
from .models import Client, Distribution, Message

admin.site.site_header = 'Сервис уведомлений'


@admin.register(Distribution)
class DistributionAdmin(admin.ModelAdmin):
    list_display = ('__str__', 'code_filter', 'tag_filter')
    search_fields = ('code_filter', 'tag_filter')
    list_filter = ('start',)
    form = DistributionForm


@admin.register(Client)
class ClientAdmin(admin.ModelAdmin):
    list_display = ('id', '__str__', 'code', 'tag')
    list_display_links = ('id', '__str__')
    search_fields = ('code', 'tag')


@admin.register(Message)
class MessageAdmin(admin.ModelAdmin):
    list_display = ('__str__', 'status', 'distribution', 'client')
    list_filter = ('status', 'created', 'distribution')
    readonly_fields = ('status', 'created', 'distribution', 'client')
