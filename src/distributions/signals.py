import json
import logging

from django.db.models import signals
from django.dispatch import receiver
from django.utils import timezone
from django_celery_beat.models import CrontabSchedule, PeriodicTask

from .models import Distribution
from .tasks import distribution_task

logger = logging.getLogger('main')


@receiver(signals.post_save, sender=Distribution)
def create_distribution_task(sender, instance, created, **kwargs):
    """ Создание задачи на рассылку
    # distribution_task.apply_async(args=[instance.id], eta=instance.start)
    """
    if not created:
        PeriodicTask.objects.filter(name__startswith=f'Task_{instance.id}').delete()
        logger.info('DISTRIBUTION_TASK|deleted old')

    if (instance.start - timezone.now()).total_seconds() <= 60:
        # запуск задачи немедленно
        distribution_task.apply_async(kwargs={'distribution_id': instance.id})
        logger.info('DISTRIBUTION_TASK|created now')
        return

    # планирование задачи
    crontab_schedule, _ = CrontabSchedule.objects.get_or_create(
        minute=str(instance.start.minute),
        hour=str(instance.start.hour),
        day_of_week=str((instance.start.weekday() + 1) % 7),
        day_of_month=str(instance.start.day),
        month_of_year=str(instance.start.month),
        timezone=timezone.get_default_timezone()
    )

    PeriodicTask.objects.create(
        name=f'Task_{instance.id} - from {timezone.localtime().strftime("%d/%m/%Y %H:%M")}',
        task='distributions.tasks.distribution_task',
        kwargs=json.dumps({'distribution_id': instance.id}),
        crontab=crontab_schedule,
        one_off=True
    )
    logger.info(f'DISTRIBUTION_TASK|created future for {instance.start.strftime("%d/%m/%Y %H:%M")}')
