import logging
from dataclasses import dataclass
from urllib.parse import urljoin

import pytz
import requests
from celery import shared_task
from django.utils import timezone

from notification_service import settings

from .models import Client, Distribution, Message

PROB_FBRQ_URL = 'https://probe.fbrq.cloud/v1/'
CHUNK_SIZE = 100

logger = logging.getLogger('main')


@dataclass
class ApiResponse:
    code: int
    message: str


@shared_task
def distribution_task(distribution_id: int):
    """ Создание задач по отправке сообщений в рамках рассылки
    """
    logger.info(f'DISTRIBUTION_TASK|started {distribution_id}')
    try:
        distribution = Distribution.objects.get(pk=distribution_id)
    except Distribution.DoesNotExist as exc:
        logger.error(f'DISTRIBUTION_TASK| {exc}')
        return {'report': str(exc)}

    # получить клиентов в рамках рассылки (match - кастомный QuerySet)
    clients = Client.objects.match(
        code=distribution.code_filter,
        tag=distribution.tag_filter
    ).iterator(chunk_size=CHUNK_SIZE)

    # сформировать задачи по рассылке
    send_message.chunks(
        (
            (
                Message.objects.create(
                    status=Message.Status.SENDING,
                    distribution=distribution,
                    client=client
                ).id,
            )
            for client in clients
        ),
        n=CHUNK_SIZE
    ).apply_async()

    logger.info(f'DISTRIBUTION_TASK|finished {distribution_id}')
    return {'report': 'OK'}


@shared_task(retry_backoff=True, max_retries=3, autoretry_for=(Exception,))
def send_message(message_id: int):
    """ Отправка сообщений через внешний сервис
    """
    logger.info(f'SEND_MESSAGE|started {message_id}')
    message = Message.objects.select_related('distribution', 'client').get(pk=message_id)

    # проверить, что рассылка еще не закончилась
    if message.distribution.end and message.distribution.end < timezone.now():
        message.status = Message.Status.MISSED_DEADLINE
        message.save()
        logger.warning(f'SEND_MESSAGE|missed_deadline {message_id}')
        return {'report': 'Missed deadline'}

    # проверить час отправки сообщения по местному времени
    client_time = timezone.now().astimezone(pytz.timezone(message.client.timezone))
    if client_time.hour < message.distribution.allowed_start_hour \
            or message.distribution.allowed_end_hour <= client_time.hour:
        message.status = Message.Status.TIMEZONE_CONFLICT
        message.save()
        logger.warning(f'SEND_MESSAGE|timezone_conflict {message_id}')
        return {'report': 'Timezone conflict'}

    headers = {
        'Authorization': f'Bearer {settings.PROB_FBRQ_TOKEN}',
        'Content-type': 'application/json',
        'Accept': 'application/json',
    }
    payload = {
        'id': message.id,
        'phone': int(str(message.client.phone_number)),
        'text': message.distribution.text
    }

    try:
        response = requests.post(urljoin(PROB_FBRQ_URL, f'send/{message.id}'),
                                 json=payload, headers=headers)
        response.raise_for_status()

        response_data = ApiResponse(**response.json())
        if response_data.code == 0 and response_data.message == 'OK':
            message.status = Message.Status.DELIVERED
            message.save()
            logger.info(f'SEND_MESSAGE|delivered {message_id}')
        else:
            raise requests.exceptions.RequestException

    except (requests.exceptions.RequestException,
            requests.exceptions.ConnectionError,
            TypeError) as exc:
        if message.status != Message.Status.FAILED:
            message.status = Message.Status.FAILED
            message.save()
        logger.error(f'SEND_MESSAGE| {exc}')
        raise

    return {'report': 'OK'}
