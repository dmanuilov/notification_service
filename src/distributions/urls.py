from django.urls import include, path
from drf_spectacular.views import SpectacularAPIView, SpectacularSwaggerView
from rest_framework import routers

from . import views

router = routers.SimpleRouter()
router.register('distributions', views.DistributionView)
router.register('clients', views.ClientView)

urlpatterns = [
    path('', include(router.urls)),
    path('statistics/', views.TotalStatisticsAPIView.as_view(), name='total-statistics'),
    path('statistics/<int:pk>', views.DetailStatisticsAPIView.as_view(), name='detail-statistics'),
    path('health', views.HealthView.as_view(), name='health'),

    path('schema/', SpectacularAPIView.as_view(), name='schema'),
    path('docs/', SpectacularSwaggerView.as_view(url_name='schema')),
]
