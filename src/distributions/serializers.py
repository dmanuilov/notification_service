from django.utils import timezone
from drf_spectacular.utils import OpenApiExample, extend_schema_serializer
from rest_framework import serializers
from rest_framework.exceptions import ValidationError

from .models import Client, Distribution


@extend_schema_serializer(
    examples=[
        OpenApiExample(
            'Пример рассылки',
            value={
                'text': 'Текст сообщения',
                'code_filter': 952,
                'tag_filter': 'тег',
                'start': timezone.localtime().strftime('%Y-%m-%dT%H:%M'),
                'end': (timezone.localtime() + timezone.timedelta(hours=2)).strftime('%Y-%m-%dT%H:%M'),
                'allowed_start_hour': 8,
                'allowed_end_hour': 22,
            },
        ),
    ]
)
class DistributionSerializer(serializers.ModelSerializer):
    class Meta:
        model = Distribution
        fields = '__all__'

    def validate(self, attrs):
        # проверка, что начало рассылки не позже окончания
        start = attrs.get('start') or timezone.localtime()
        end = attrs.get('end')
        if end and start > end:
            raise ValidationError('Убедитесь, что начало рассылки раньше окончания.')
        return attrs


@extend_schema_serializer(
    examples=[
        OpenApiExample(
            'Пример клиента',
            value={
                'phone_number': 79523609280,
                'code': 952,
                'tag': 'тег',
                'timezone': 'Europe/Moscow',
            },
        ),
    ]
)
class ClientSerializer(serializers.ModelSerializer):
    class Meta:
        model = Client
        fields = '__all__'
