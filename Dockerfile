FROM python:3.11-slim

WORKDIR /opt/notification_service

ENV PYTHONUNBUFFERED=1

RUN apt-get update && apt-get install curl build-essential libpq-dev -y && \
    pip install --upgrade pip && \
    pip install poetry

COPY pyproject.toml ./
RUN poetry config virtualenvs.create false && \
    poetry install --without dev --no-ansi --no-interaction --no-root

COPY src/ ./

ENTRYPOINT ["bash", "entrypoint.sh"]

CMD ["gunicorn", "notification_service.wsgi:application", "-w", "2", "-b", "0.0.0.0:8000"]
